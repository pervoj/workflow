# window.py
#
# Copyright 2019 cunidev
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, GLib

from datetime import date, datetime
from traceback import print_exc
import json
from .graph import GraphDrawingArea
from .simpleaw import SimpleAW
from . import timeutils
from .details_dialog import DetailsDialog
from .about_dialog import AboutDialog

#@Gtk.Template(resource_path='/com/gitlab/cunidev/Workflow/window.ui')
class WorkflowWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'WorkflowWindow'
    _version = '?'

    events_db = []

    EMPTY_STATUS_EMPTY = 0
    EMPTY_STATUS_ERROR = 1

    REFRESH_INTERVAL = 30 # seconds
    selected_date = date.today()

    ranking = {}
    total_time = 0.0


    s = SimpleAW()

    list = Gtk.ListBox()
    popover_sync = Gtk.Popover()
    popover_date = Gtk.Popover()
    graph = GraphDrawingArea([])
    calendar = Gtk.Calendar() # todo: replace this awful widget
    sync_label = Gtk.Label(margin = 10)
    bar = Gtk.HeaderBar()
    empty = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing = 20, valign = Gtk.Align.CENTER)
    empty_label = Gtk.Label()


    def __init__(self, version, **kwargs):
        super().__init__(**kwargs)
        self._version = version
        self.set_default_size(600,400)

        # HeaderBar
        self.bar.set_show_close_button(True)
        self.bar.set_title("Workflow")
        self.set_titlebar(self.bar)

        self.graph.set_name("date-picker")

        # HeaderBar date button
        btn_date = Gtk.Button.new_from_icon_name("x-office-calendar-symbolic", Gtk.IconSize.SMALL_TOOLBAR)
        btn_date.set_tooltip_text("Choose date")
        btn_date.connect("clicked", self.toggle_date_popover)
        self.bar.pack_start(btn_date)

        # HeaderBar calendar popover
        self.popover_date.set_position(Gtk.PositionType.BOTTOM)
        self.calendar.set_display_options(Gtk.CalendarDisplayOptions.SHOW_HEADING | Gtk.CalendarDisplayOptions.SHOW_DAY_NAMES)
        self.calendar.connect("day-selected", self.set_date)
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        box.add(self.graph)
        box.add(self.calendar)

        # HeaderBar about button
        btn_about = Gtk.Button.new_from_icon_name("help-about-symbolic", Gtk.IconSize.SMALL_TOOLBAR)
        btn_about.set_tooltip_text("About")
        btn_about.connect("clicked", self.show_about_dialog)
        self.bar.pack_end(btn_about)

        self.popover_date.add(box)

        self.sync_label.use_markup = True
        self.popover_sync.set_position(Gtk.PositionType.BOTTOM)
        self.popover_sync.add(self.sync_label)
        self.popover_sync.padding = 20

        # empty status
        self.empty.set_visible(True)

        self.empty_icon = Gtk.Image.new_from_icon_name("airplane-mode", Gtk.IconSize.DIALOG)
        self.empty_icon.set_pixel_size(128)
        self.empty_icon.set_visible(True)
        self.empty.pack_start(self.empty_icon, False, False, 0)

        self.empty_label.use_markup = True
        self.empty_label.set_visible(True)
        self.empty.pack_start(self.empty_label, False, False, 0)
        self._set_empty_status(self.EMPTY_STATUS_EMPTY)


        # Main ListBox
        self.list.set_placeholder(self.empty)
        self.list.set_selection_mode(Gtk.SelectionMode.NONE)
        self.list.connect("row-activated", self.show_details)

        # Main view
        scroll_area = Gtk.ScrolledWindow()
        scroll_area.add(self.list)

        self.add(scroll_area)

        self.show_all()

        # enable refresh loop
        self._refresh_loop()

    def _refresh_loop(self):
        self.refresh()
        GLib.timeout_add(self.REFRESH_INTERVAL * 1000, self._refresh_loop)

    def refresh(self, button = None):
        self._list_empty()

        if self.selected_date == None:
            date_raw = self.calendar.get_date()
            self.selected_date = date(year = date_raw[0], month = date_raw[1] + 1, day = date_raw[2])


        error = False

        try:
            self.events_db = self.s.get_ranking_by_date_range(self.s.get_bucket_name(), self.selected_date, self.selected_date)
        except Exception as e:
            print_exc()
            error = True
            self._set_empty_status(self.EMPTY_STATUS_ERROR)

        if not error:
            weekly_usage = self.s.get_weekly_usage(self.s.get_bucket_name(), self.selected_date)

            # TODO: Gtk.Revealer instead of ugly set_visible!
            if weekly_usage == [0, 0, 0, 0, 0, 0, 0]:
                self.graph.set_visible(False)
            else:
                self.graph.set_visible(True)
                self.graph.set_day_list(weekly_usage)

        (self.ranking, self.total_time) = self.s.get_ranking_from_list(self.events_db, self.s.RANKING_APP_NAME)

        if self.ranking == {} and not error:
            self._set_empty_status(self.EMPTY_STATUS_EMPTY)

        for name, record in sorted(self.ranking.items(), key=lambda item: item[1], reverse=True):
            if(record < 10): # avoid records with less than 10s total screen time; todo: make high pass filter configurable
                continue

            row = Gtk.ListBoxRow()
            v_box = Gtk.Box(spacing=0, margin=15, orientation=Gtk.Orientation.HORIZONTAL)

            image = Gtk.Image.new_from_icon_name(name, Gtk.IconSize.DND)
            v_box.pack_start(image, False, False, 10)

            label = Gtk.Label()
            label.use_markup = True
            label.set_markup(name)
            v_box.pack_start(label, False, True, 10)

            label = Gtk.Label()
            label.use_markup = True
            label.set_markup("<span foreground=\"grey\"><i>" + timeutils.format_seconds(record) + "</i></span>")
            v_box.pack_end(label, False, False , 10)

            row.add(v_box)
            self.list.add(row)

        self.bar.set_subtitle("Total time: " + timeutils.format_seconds(self.total_time))
        self.list.show_all()

    def show_details(self, listbox, row):
        app_name = row.get_child().get_children()[1].get_text() # awful
        app_events_ranking = self.s.get_ranking_from_list(self.events_db, self.s.RANKING_WIN_TITLE, app_name)[0]
        dialog = DetailsDialog(app_events_ranking)
        dialog.set_title("Usage for " + app_name)
        dialog.set_modal(True)
        dialog.set_transient_for(self)
        dialog.show()

    def toggle_date_popover(self, button):
        self.popover_date.set_relative_to(button)
        self.popover_date.popup()
        self.popover_date.show_all()

    def set_date(self, calendar):
        date_raw = calendar.get_date()
        self.selected_date = date(year = date_raw[0], month = date_raw[1] + 1, day = date_raw[2])
        if self.selected_date > date.today():
            calendar.select_month(date.today().month - 1, date.today().year)
            calendar.select_day(date.today().day)
            self.selected_date = date.today()
            return
        self.refresh()

    def show_about_dialog(self, button):
        d = AboutDialog(self, self._version)
        d.set_modal(True)
        d.set_transient_for(self)
        d.show()

    def _set_empty_status(self, status):
        if status == self.EMPTY_STATUS_EMPTY:
            self.empty_label.set_markup("<span foreground=\"gray\" size=\"medium\" weight=\"semibold\">No usage data for today.</span>")
        elif status == self.EMPTY_STATUS_ERROR:
            self.empty_label.set_markup("<span foreground=\"gray\" size=\"medium\" weight=\"semibold\">Error fetching usage data</span>\n\n" + \
            	"<span foreground=\"gray\" size=\"small\" weight=\"normal\">Is ActivityWatch installed properly?</span>")

        self.empty.show_all()

    def _list_empty(self):
        for child in self.list.get_children():
            child.destroy()
        self.list.show_all()







