# main.py
#
# Copyright 2019 cunidev
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gio, Gdk

from .window import WorkflowWindow

class Application(Gtk.Application):
    _version = '?'
    def __init__(self, version):
        super().__init__(application_id='com.gitlab.cunidev.Workflow',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)
        self._version = version
        self._set_css()

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = WorkflowWindow(self._version, application=self)
        win.present()

    def _set_css(self):
        css = b"""calendar { padding: 2px; border-radius: 8px; }""" # todo: add proper CSS file
        css_provider = Gtk.CssProvider()
        css_provider.load_from_data(css)
        context = Gtk.StyleContext()
        screen = Gdk.Screen.get_default()
        context.add_provider_for_screen(screen, css_provider,
                                    Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

def main(version):
    app = Application(version)
    return app.run(sys.argv)
